import {FilterData} from "./FilterData";
import * as React from "react";
import {ChangeEvent, FormEvent, SyntheticEvent} from "react";


interface Porps {
    onFilterChange: (data: FilterData) => void
}

//we dont need store any state because input updates itself
// and we use event value to propagate it into parent
class State {

}

export class HotelsListFilter extends React.Component<Porps, State> {
    data: FilterData;
    constructor(props: Porps) {
        super(props);
        this.data = new FilterData("");

        this.statusChanged = this.statusChanged.bind(this);
    }

    statusChanged(e: FormEvent<HTMLInputElement>){
        this.data.name = e.currentTarget.value
        this.props.onFilterChange(this.data);
    }

    render() {
        return (
            <div>
                <div>
                    <span>Name: </span>
                    <input onChange={this.statusChanged} />
                </div>
            </div>
        )
    }
}
