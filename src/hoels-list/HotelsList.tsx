import * as React from "react";
import {HotelsListItem} from "./HotelsListItem";
import {HotelsListElement} from "./HotelsListElement";
import {FilterData} from "./FilterData";
import {HotelsListFilter} from "./HotelsListFilter";
import HotelDetails from "./HotelDetails";

interface State {
    items: HotelsListItem[];
    originalItems: HotelsListItem[];
    details?: HotelsListItem;
}

export class HotelsList extends React.Component<{}, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            originalItems: this.getHotels(),
            items: this.getHotels(),
        };
    }

    showDetails = (hotel: HotelsListItem) => {
        this.setState({details: hotel});
    };

    filterChanged = (data: FilterData) => {

        console.log('filter changed => ', data);
        const items = this.state.originalItems.filter(item =>
            item.name.toLowerCase().includes(data.name.toLowerCase()));

        //see more about object cloning: https://stackoverflow.com/questions/28150967/typescript-cloning-object
        this.setState(Object.assign({}, this.state, {items: items}));
    };

    private getHotels() {
        return [
            new HotelsListItem('Ibis Budget Ochota'),
            new HotelsListItem('Ibis Budget Center'),
            new HotelsListItem('Marriott center'),
            new HotelsListItem('Small Hotel')
        ];
    }


    render() {
        return (
            <div>
                <HotelsListFilter onFilterChange={this.filterChanged}/>

                {this.state.items.map(hotel =>
                    <div>
                        <HotelsListElement data={hotel} onShowDetails={this.showDetails}/>
                    </div>
                )}

                {this.details()}
            </div>
        )
    }

    private details(): any {
        if (this.state.details) {
            return <HotelDetails hotel={this.state.details}/>
        } else {
             return null;
        }
    }
}
