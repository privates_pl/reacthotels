import React from "react";
import {HotelsListItem} from "./model/HotelsListItem";

interface Props {
    hotel: HotelsListItem;
}

const HotelDetails: React.FunctionComponent<Props> = (props) => {
    return <div>
        <div>{props.hotel ? props.hotel.name: ''}</div>
    </div>;
};

export default HotelDetails;