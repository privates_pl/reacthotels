export class FilterData{
    name: string;


    constructor(name: string) {
        this.name = name;
    }

    isEmpty(): boolean {
        return this.name == null || this.name.length === 0;
    }
}
