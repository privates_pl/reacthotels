import * as React from "react";
import {HotelsListItem} from "./HotelsListItem";

interface Props {
    data: HotelsListItem;
    onShowDetails: any;
}

interface State {
}

export class HotelsListElement extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        // this.showDetails = this.showDetails.bind(this);
    }

    showDetails = () => {
        this.props.onShowDetails(this.props.data);
    };

    render() {
        return <div>
            <span> {this.props.data.name}</span>
            <button onClick={this.showDetails}>details</button>
        </div>
    }
}
