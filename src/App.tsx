import React from 'react';
import './App.css';
import {HotelsList} from "./hoels-list/HotelsList";

const App: React.FC = () => {
  return (
    <div className="App">
     <HotelsList/>
    </div>
  );
};

export default App;
